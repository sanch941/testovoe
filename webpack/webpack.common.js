const {
    webpackPaths: {  sourcePath }
} = require('./paths');


module.exports = {
    context: sourcePath,
    target: 'web',
    resolve: {
        extensions: ['.js'],
        mainFields: ['module', 'browser', 'main']      
    },
    
};
