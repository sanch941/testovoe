const path = require('path');

const rootDir = path.join(__dirname, '..');
const sourcePath = path.join(rootDir, 'src');
const outputPath = path.join(rootDir, 'dist');

module.exports.webpackPaths = {
    sourcePath,
    outputPath,
    rootDir
};
