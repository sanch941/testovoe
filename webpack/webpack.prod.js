const { merge } = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const {
    webpackPaths: { outputPath, sourcePath, rootDir }
} = require('./paths');



module.exports = merge(
    common,
    {
        entry: path.join(sourcePath, 'index.js'),
        mode: 'production',
        devtool: 'hidden-source-map',
        devServer: {
            contentBase: outputPath,
            hot: true,
            inline: true,
            overlay: true,           
            stats: 'minimal',
            clientLogLevel: 'warning'
        },      
        target: 'web',
        output: {
            path: outputPath,            
            filename: 'bundle.js',            
        },
        optimization: {
            minimize: true,
            minimizer: [
                new TerserPlugin({
                    parallel: true,                    
                    terserOptions: {                                                
                        output: {
                            comments: false
                        }
                    }
                })
            ]               
        },
        module: {
            rules: [
                {
                    test: /\.(js)?$/,
                    use: [                       
                        {
                            loader: 'babel-loader'                          
                        }
                    ],
                    exclude: /node_modules/
                },                           
                { test: /\.html$/, use: 'html-loader' },
                {
                    test: /\.(png|jpe?g|gif|svg)$/i,
                    exclude: /\.inline.svg$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name() {
                                    return '[path][name].[ext]';
                                },
                                esModule: false
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: ['file-loader']
                }              
            ]
        },
        plugins: [
            new CleanWebpackPlugin(),                    
            new HtmlWebpackPlugin({
                template: path.join(sourcePath, 'index.html'),
                minify: {
                    minifyJS: true,
                    minifyCSS: true,
                    removeComments: true,
                    useShortDoctype: true,
                    collapseWhitespace: true,
                    collapseInlineTagWhitespace: true
                }
            })
        ],
        performance: {
            hints: false
        }
    }
);
