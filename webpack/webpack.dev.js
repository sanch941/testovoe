const { merge } = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {
    webpackPaths: {  sourcePath }
} = require('./paths');

module.exports = merge(
    common,
    {
        entry: path.resolve(sourcePath, 'index.js'),
        mode: 'development',
        devtool: 'cheap-module-eval-source-map',
        devServer: {
            port: 3001,
            hot: true,
            contentBase: path.join(sourcePath),
            open: true,
            inline: true,
            overlay: true,                    
            clientLogLevel: 'none'
        },        
        module: {
            rules: [
                {
                    test: /\.js?$/,
                    use: [                  
                        {
                            loader: 'babel-loader'                         
                        }
                    ],
                    exclude: /node_modules/
                },                             
                { test: /\.html$/, use: 'html-loader' },
                {
                    test: /\.(png|jpe?g|gif|svg)$/i,
                    exclude: /\.inline.svg$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name() {
                                    return '[path][name].[ext]';
                                },
                                esModule: false
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: ['file-loader']
                }              
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: path.join(sourcePath, 'index.html')
            })                     
        ],
        performance: {
            hints: false
        }
    }
);
